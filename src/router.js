import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/login.vue';
import Home from './views/home.vue';

import Usuarios from './components/usuarios/app_usuarios.vue';
import addUsuarios from './components/usuarios/app_add_Usuarios.vue';
import updateUsuarios from './components/usuarios/app_update_Usuarios.vue';

import Vendedores from './components/vendedores/app_vendedores.vue';
import addVendedores from './components/vendedores/app_add_vendedor.vue';
import updateVendedores from './components/vendedores/app_update_vendedor.vue';

import addVista from './components/vistasH/app_add_vista.vue';
import Vistas from './components/vistasH/app_vistas.vue';
import updateVista from './components/vistasH/app_update_Vista.vue';

import Amenities from './components/amenities/app_amenities.vue';
import addAmenitie from './components/amenities/app_add_amenitie.vue';
import updateAmenitie from './components/amenities/app_update_Amenitie.vue';

import EspDepartamento from './components/espacioDept/app_espacioDpt.vue';
import addEspacio from './components/espacioDept/app_add_espacioDpt.vue';
import updateEspacio from './components/espacioDept/app_Update_espacioDpt.vue';

import EstatusDepartamento from './components/estatusDepartamento/app_estatusDepartamento.vue';
import addEstatusDepartamento from './components/estatusDepartamento/app_add_estatusDepartamento.vue';
import updateEstatusDepartamento from './components/estatusDepartamento/app_update_estatusDepartamento.vue';

import Departamentos from './components/departamentos/app_departamentos.vue';
import addDepartamento from './components/departamentos/app_add_Departamentos.vue';
import updateDepartamento from './components/departamentos/app_update_departamentos.vue';

import tipoUsuario from './components/tipoUsuario/app_tiposUsuario.vue';
import addTipoUsuario from './components/tipoUsuario/app_add_tipoUsuario.vue';
import updateTipoUsuario from './components/tipoUsuario/app_update_tipoUsuario.vue';

import  Estados from'./components/estados/app_estados.vue';
import addEstado from './components/estados/app_add_estados.vue';
import updateEstado from './components/estados/app_update_estados.vue';

import Colonias from'./components/colonias/app_colonias.vue';
import addColonias from './components/colonias/app_add_colonias.vue';
import updateColonias from './components/colonias/app_update_colonias.vue';

import Localidades from'./components/localidades/app_localidades.vue';
import addLocalidades from './components/localidades/app_add_localidades.vue';
import updateLocalidades from './components/localidades/app_update_localidades.vue';


import Municipios from './components/municipios/app_municipio.vue';
import municipiosAdd from './components/municipios/app_add_municipio.vue';
import municipiosUpdate from './components/municipios/app_update_municipio.vue';

import ciudades from './components/ciudades/app_ciudades.vue';
import ciudadEdita from './components/ciudades/aapEditCiudad.vue';
import ciudadAgrega from './components/ciudades/appAddCiudad.vue';


import status from './components/status/app_status.vue';
import statusAdd from './components/status/app_add_status.vue';
import statusUpdate from './components/status/app_update_status.vue';

import Torre from './components/torres/app_torres.vue';
import TorreAdd  from './components/torres/app_add_torre.vue'
import TorreUpdate  from './components/torres/app_update_torre.vue'



import Cliente from './components/clientes/app_clientes.vue'
import ClienteAdd from './components/clientes/app_add_cliente.vue'
import ClienteUpdate from './components/clientes/app_update_cliente.vue'
import ClienteAddComercial from './components/clientes/app_add_clientecomercial.vue'
import ClienteComercialUpdate from './components/clientes/app_update_clientecomercial.vue'


import calles from './components/calles/app_calles.vue';
import callesAdd from './components/calles/app_add_calles.vue';
import calleUpdate from './components/calles/app_update_calles.vue';


import Servicios from './components/servicios/app_servicios.vue'
import addServicios from './components/servicios/app_add_servicio.vue'
import updateServicios from './components/servicios/app_update_servicio.vue'


import Permisos from './components/permisos/permisos.vue';

import Cotizacion from './components/cotizaciones/app_cotizacion.vue';
import addCotizacion from './components/cotizaciones/app_add_cotizacion.vue';
import updateCotizacion from './components/cotizaciones/app_update_cotizacion.vue';

import Configuracion from './components/configuracion_sistema/app_configuracion.vue';
import addConfiguracion from './components/configuracion_sistema/app_add_configuracion.vue';
import UpdateConfiguracion from './components/configuracion_sistema/app_update_configuracion.vue';

import AsignacionDepartamentos from './components/asignacionDepartamentos/app_asignacion_departamento.vue';
import AddAsignacionDepartamentos from './components/asignacionDepartamentos/app_add_asignacion_departamento.vue';

import Motivos from './components/CancelacionCotizacion/app_motivos.vue';
import addMotivos from './components/CancelacionCotizacion/app_add_motivo.vue';
import UpdateMotivos from './components/CancelacionCotizacion/app_update_motivo.vue';

import cuentasBancarias from './components/cuentasBancarias/app_cuentasBancarias.vue';
import addCuentasBancarias from './components/cuentasBancarias/app_add_cuentasBancarias.vue';
import updateCuentasBancarias from './components/cuentasBancarias/app_update_cuentasBancarias.vue';
import Bancos from './components/bancos/app_bancos.vue'
import addBancos from './components/bancos/app_add_bancos.vue'
import updateBancos from './components/bancos/app_update_bancos.vue'
import conceptosDePago from './components/conceptosDePago/app_conceptosDePago.vue';
import addConceptosDePago from './components/conceptosDePago/app_add_conceptosDePago.vue';
import updateConceptosDePago from './components/conceptosDePago/app_update_conceptosDePago.vue';

import contrato from './components/contrato/app_contrato.vue';
import addContrato from './components/contrato/app_add_contrato.vue';

import asignacionCompradoresWEB from './components/asignacionCompradoresWEB/app_asignacionCompradoresWEB.vue';

import canvasBodegas from '../src/views/bodegas.vue'
import canvasCajones from '../src/views/Cajones.vue'

import agenda from './components/agenda/app_agenda.vue';
import addAgenda from './components/agenda/app_add_agenda.vue';
import updateAgenda from './components/agenda/app_update_agenda.vue';


import tipoVendedor from './components/tiposVendedores/app_tipoVendedor.vue'
import AddtipoVendedor from './components/tiposVendedores/app_add_tipoVendedor.vue'


import reporteDepVendidosApartados from './components/reportes/reporteDepVendidosApartados.vue'
import reporteEstatusDepartamento from './components/reportes/reporteEstatusDepartamento.vue'
import reporteResumenEstatusDep from './components/reportes/reporteResumenEstatusDep.vue'
import reporteListaCliente from './components/reportes/reporteListClientes.vue'
import reporteResumenCliente from './components/reportes/reporteResumenClientes.vue'
import reporteAgendasPorHacer from './components/reportes/reporteVisitaLLamadasPorHacer.vue'
import reporteAgendasEjecutadas from './components/reportes/reporteVisitaLLamadasEjecutadas.vue'
import reporteIngresoPago from './components/reportes/reporteDeIngresos.vue'

import reporteClientesVencidos from './components/reportes/reporteClientesVencidos.vue'
import reporteClientesPorVencer from './components/reportes/reporteClientesPorVencer.vue'
import reporteEstadosCuentaEnviados from './components/reportes/reporteEstadosCuentaEnviados.vue'

import reporteIngresoPorCobrar from './components/reportes/reporteDeIngresosPorCobrar.vue'
import reporteVentasVendedor from './components/reportes/reporteVentasVendedor.vue'
import reporteDeIngresosPorCobrar from './components/reportes/reporteDeIngresosPorCobrar.vue'
import reportePrecioVentaDepartamento from './components/reportes/reportePrecioVentaDepartamento.vue';
import reporteGeneralIngresos from './components/reportes/reporteGeneralIngresos.vue'
import reporteTarea from './components/reportes/reporteTareas.vue'

import MotivosCancelacion from './components/CancelacionPago/app_motivosCancelacionPago.vue'
import addMotivosCancelacion from './components/CancelacionPago/app_add_motivoCancelacionPago.vue'
import UpdateMotivosCancelacion from './components/CancelacionPago/app_update_motivoCancelacionPago.vue'


import recepcionDePagos from './components/recepcionPagos/app_recepcionDePagos.vue';
import addRecepcionDePagos from './components/recepcionPagos/app_add_recepcionDePagos.vue';
import updateRecepcionDePagos from './components/recepcionPagos/app_update_recepcionDePagos.vue';

import AddPlanoBodegaCajon from './components/planos/app_add_nombre_plano.vue';
import CancelaPago from './components/CancelaPago/app_CancelacionPago.vue'

import DetalleEstatusDepartamento from './components/estatusDepartamento/app_detalle.vue';

import addMediosDifusion from './components/mediosDeDifusion/app_add_mediosDifusion.vue';
import updateMediosDifusion from './components/mediosDeDifusion/app_update_mediosDifusion.vue';
import MediosDifusion from './components/mediosDeDifusion/app_mediosDifusion.vue';

import addMedioContacto from './components/medioContacto/app_add_medioContacto.vue';
import updateMedioContacto from './components/medioContacto/app_update_medioContacto.vue';
import MedioContacto from './components/medioContacto/app_medioContacto.vue';

import Olt from './components/Olt/app_olt.vue'
import addOlt from './components/Olt/app_add_olt.vue'
import updateOlt  from './components/Olt/app_update_olt.vue'

import marca from './components/marca/app_marca.vue';
import addMarca from './components/marca/app_add_marca.vue';
import updateMarca from './components/marca/app_update_marca.vue';

import modelo from './components/modelo/app_modelo.vue';
import addModelo from './components/modelo/app_add_modelo.vue';
import updateModelo from './components/modelo/app_update_modelo.vue';

import firmware from './components/firmware/app_firmware.vue';
import addFirmware from './components/firmware/app_add_firmware.vue';
import updatefirmware from './components/firmware/app_update_firmware.vue';

import EstadoDeCuenta from './components/EstadosDecuenta/EstadosDeCuentaEnviar.vue';

import Response from './components/Response/Response.vue';


import VueLocalStorage from 'vue-localstorage';
Vue.use(VueLocalStorage);
Vue.use(Router);


const router=new Router({
  /* mode: 'history',
  base: process.env.BASE_URL, */
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/bodegas/:id/:ReadOnly?',
      name: 'canvasBodegas',
      component: canvasBodegas
    },   

    {
      path: '/cajones/:id/:ReadOnly?',
      name: 'canvasCajones',
      component: canvasCajones
    },   
    {
      path: '/home',
      name: 'home',
      component: Home,
      children: [

        { path: '/estadoscuenta', name: 'EstadosCuenta', component: EstadoDeCuenta },

        { path: '/response', name: 'Response', component: Response },
        
        


        { path: '/firmware', name: 'firmware', component: firmware },
        { path: '/firmware/add', name: 'addFirmware', component: addFirmware },
        
        { path: '/firmware/update/:id', name: 'updatefirmware', component: updatefirmware },
        
        { path: '/modelo', name: 'modelo', component: modelo },
        { path: '/modelo/add', name: 'addModelo', component: addModelo },
        { path: '/modelo/update/:id', name: 'updateModelo', component: updateModelo },
        

        { path: '/marca', name: 'marca', component: marca },
        { path: '/marca/add', name: 'addMarca', component: addMarca },
        { path: '/marca/update/:id', name: 'updateMarca', component: updateMarca },
        
        
        
        { path: '/status', name: 'status', component: status },
        { path: '/status/add', name: 'statusAdd', component: statusAdd },
        { path: '/status/update/:id', name: 'statusUpdate', component: statusUpdate },
        
        
      
        

        { path: '/calles', name: 'calles', component: calles },
        { path: '/calles/add', name: 'callesAdd', component: callesAdd },
        { path: '/calles/update/:id', name: 'calleUpdate', component: calleUpdate },
        
        
        
         {path: '/ciudades', name: 'ciudades', component: ciudades },
         { path: '/ciudades/add', name: 'ciudadAgrega', component: ciudadAgrega },
         { path: '/ciudades/edit/:id', name: 'ciudadEdita', component: ciudadEdita },
     
        

        {path: '/detalleEstatusdepartamento', name: 'detalleestatusDepartamento', component: DetalleEstatusDepartamento},

        {path: '/recepcionDePagos', name: 'recepcionDePagos', component: recepcionDePagos},
        {path: '/recepcionDePagos/add', name: 'addRecepcionDePagos', component: addRecepcionDePagos},
        {path: '/recepcionDePagos/update/:id', name: 'updateRecepcionDePagos', component: updateRecepcionDePagos},


        {path: '/asignacionCompradoresWEB', name: 'asignacionCompradoresWEB', component: asignacionCompradoresWEB},

        {path: '/agenda', name: 'agenda', component: agenda},
        {path: '/agenda/add', name: 'addAgenda', component: addAgenda},
        {path: '/agenda/update/:id/:cliente?', name: 'updateAgenda', component: updateAgenda},

        {path: '/contrato', name: 'contrato', component: contrato},
        {path: '/contrato/add', name: 'addContrato', component: addContrato},
        {path: '/contrato/update/:id', name: 'editContrato', component: addContrato},

        {path: '/cuentasBancarias', name: 'cuentasBancarias', component: cuentasBancarias},
        {path: '/cuentasBancarias/add', name: 'addCuentasBancarias', component: addCuentasBancarias},
        {path: '/cuentasBancarias/update/:id', name: 'updateCuentasBancarias', component: updateCuentasBancarias},

        {path: '/bancos', name: 'bancos', component: Bancos},
        {path: '/bancos/add', name: 'addBancos', component: addBancos},
        {path: '/bancos/update/:id', name: 'updateBancos', component: updateBancos},

        {path: '/conceptosDePago', name: 'conceptosDePago', component: conceptosDePago},
        {path: '/conceptosDePago/add', name: 'addConceptosDePago', component: addConceptosDePago},
        {path: '/conceptosDePago/update/:id', name: 'updateConceptosDePago', component: updateConceptosDePago},
        {path: '/asignacionDepartamentos', name: 'asignacionDepartamentos', component: AsignacionDepartamentos},
        {path: '/asignacionDepartamentos/add/:id', name: 'AddasignacionDepartamentos', component: AddAsignacionDepartamentos},
        {path: '/vendedores', name: 'vendedores', component: Vendedores },
        {path: '/vendedores/add', name: 'addVendedores', component: addVendedores },
        {path: '/vendedores/update/:id', name: 'updateVendedores', component: updateVendedores},

        {path: '/usuarios', name: 'usuarios',  component: Usuarios },
        {path: '/usuarios/add', name: 'addUsuarios', component: addUsuarios },
        {path: '/usuarios/update/:id', name: 'updateUsuarios', component: addUsuarios},

        {path: '/amenities', name: 'amenities', component: Amenities},
        {path: '/amenitie/add', name: 'addAmenities', component: addAmenitie },
        {path: '/amenitie/update/:id', name: 'updateAmenitie', component: updateAmenitie},

        {path: '/vistas', name: 'vistas', component: Vistas}, 
        {path: '/vistas/add', name: 'addVistas', component: addVista },
        {path: '/vistas/update/:id', name: 'updateVista', component: updateVista},

        {path: '/esdepartamento', name: 'espacioD', component: EspDepartamento},
        {path: '/esdepartamento/add', name: 'addEspacio', component: addEspacio },
        {path: '/esdepartamento/update/:id', name: 'updateEspacio', component: updateEspacio },
       
       
        {path: '/estatusDepartamento', name: 'estatusDepartamento',  component: EstatusDepartamento },
        {path: '/estatusDepartamento/add', name: 'addEstatusDepartamento',  component: addEstatusDepartamento },
        {path: '/estatusDepartamento/update/:id', name: 'updateEstatusDepartamento',  component: updateEstatusDepartamento },

        {path: '/departamento', name: 'departamento', component: Departamentos},
        {path: '/departamento/add', name: 'addDepartamento',  component: addDepartamento },
        {path: '/departamento/update/:id', name: 'updateDepartamento',  component: updateDepartamento },

     

        {path: '/servicios', name: 'servicios',  component: Servicios },
        {path: '/servicios/add', name: 'addServicios',  component: addServicios },
        {path: '/servicios/update/:id', name: 'updateServicios',  component: updateServicios },

        {path: '/cotizacion', name: 'cotizacion',  component: Cotizacion },
        {path: '/cotizacion/add', name: 'addCotizacion',  component: addCotizacion },
        {path: '/cotizacion/update/:id', name: 'updateCotizacion',  component: addCotizacion },

        {path: '/configuracion', name: 'configuracion',  component: Configuracion },
        {path: '/configuracion/add', name: 'addConfiguracion',  component: addConfiguracion },
        {path: '/configuracion/update/:id', name: 'UpdateConfiguracion',  component: UpdateConfiguracion },


        {path: '/tipovendedor', name: 'tipovendedor',  component: tipoVendedor },
        {path: '/tipovendedor/add', name: 'Addtipovendedor',  component: AddtipoVendedor },
        {path: '/tipovendedor/update/:id', name: 'Updatetipovendedor',  component: AddtipoVendedor },
        
        {path: '/motivos', name: 'motivos',  component: Motivos },
        {path: '/motivos/add', name: 'addMotivos',  component: addMotivos },
        {path: '/motivos/update/:id', name: 'UpdateMotivos',  component: UpdateMotivos },

        {path: '/estado', name: 'estados', component: Estados},
        {path: '/estado/add', name: 'addEstado',  component: addEstado },
        {path: '/estado/update/:id', name: 'updateEstado',  component: updateEstado },
        {path: '/colonia', name: 'colonia', component: Colonias},
        {path: '/colonia/update/:id', name: 'updateColonias',  component: updateColonias },
        {path: '/colonia/add', name: 'addColonia',  component: addColonias },
        {path: '/localidad', name: 'localidad', component: Localidades},
        {path: '/localidad/add', name: 'addLocalidad',  component: addLocalidades },
        {path: '/localidad/update/:id', name: 'updateLocalidades',  component: updateLocalidades },
        {path: '/tipoUsuario', name: 'tipoUsuario', component: tipoUsuario},
        {path: '/tipoUsuario/add', name: 'addTipoUsuario', component: addTipoUsuario},
        {path: '/tipoUsuario/update/:id', name: 'updateTipoUsuario', component: updateTipoUsuario},
        
        { path: '/municipios', name: 'municipios', component: Municipios },
        {path: '/municipios/add', name: 'municipiosAdd', component: municipiosAdd},
        { path: '/municipios/update/:id', name: 'municipiosUpdate', component: municipiosUpdate },
       
       

        {path: '/torres', name: 'torres', component: Torre},     
         {path: '/torre/add', name: 'torreAdd', component: TorreAdd},  
         {path: '/torre/update/:id', name: 'torreUpdate', component: TorreUpdate},
         {path: '/cliente', name: 'cliente', component: Cliente},
         {path: '/cliente/update/:id', name: 'ClienteUpdate', component: ClienteUpdate},
         {path: '/cliente/add', name: 'clienteAdd', component: ClienteAdd},
         {path: '/clientecomercial/add', name:'ClienteAddComercial', component:ClienteAddComercial},
         {path: '/clientecomercial/update/:id', name:'ClienteComercialUpdate', component:ClienteComercialUpdate},
         {path: '/permisos', name: 'permisos', component: Permisos},
        
         
         {path: '/reporteresumenvendidos', name: 'reporteresumenvendidos',  component: reporteDepVendidosApartados },
         {path: '/reportesestatusdepartamento', name: 'reportesestatusdepartamento',  component: reporteEstatusDepartamento },
         {path: '/reporteresumendepartamento', name: 'reporteresumendepartamento',  component: reporteResumenEstatusDep },
         {path: '/reportelistclientes', name: 'reportelistclientes',  component: reporteListaCliente },
         {path: '/reporteresumenclientes', name: 'reporteresumenclientes',  component: reporteResumenCliente },
         {path: '/reporteagendasporhacer', name: 'reporteagendasporhacer',  component: reporteAgendasPorHacer },
         {path: '/reporteagendasejecutadas', name: 'reporteagendasejecutadas',  component: reporteAgendasEjecutadas },
         {path: '/reporteingresopago', name: 'reporteingresopago',  component: reporteIngresoPago },
         {path: '/reporteingresoporcobrar', name: 'reporteingresoporcobrar',  component: reporteIngresoPorCobrar },
         {path: '/reporteventasvendedor', name: 'reporteventasvendedor',  component: reporteVentasVendedor },
         {path: '/reportedeingresosporcobrar', name: 'reportedeingresosporcobrar',  component: reporteDeIngresosPorCobrar },
         {path: '/reportePrecioVentaDepartamento', name: 'reportePrecioVentaDepartamento',  component: reportePrecioVentaDepartamento },
         {path: '/reportegeneraldeingresos', name: 'reportegeneraldeingreos',  component: reporteGeneralIngresos },
         {path: '/reportetarea', name: 'reportetarea', component: reporteTarea },
         { path: '/reporteclientesvencidos', name: 'reporteclientesvencidos', component: reporteClientesVencidos },
         { path: '/reporteclientesporvencer', name: 'reporteclientesporvencer', component: reporteClientesPorVencer },
         { path: '/reporteEstadosCuentaEnviados', name: 'reporteEstadosCuentaEnviados', component: reporteEstadosCuentaEnviados},
         

         
       

         

         
         {path: '/motivoscancelacionpago', name: 'MotivosCancelacionPago',  component: MotivosCancelacion },
         {path: '/motivoscancelacionpago/add', name: 'addMotivosCancelacionPago',  component: addMotivosCancelacion },
         {path: '/motivoscancelacionpago/update/:id', name: 'UpdateMotivosCancelacionPago',  component: UpdateMotivosCancelacion },
         

         {path: '/addPlanoBodegaCajon', name: 'addPlanoBodegaCajon',  component: AddPlanoBodegaCajon },
         {path: '/cancelapago', name: 'cancelapago',  component: CancelaPago },    
         
         {path: '/mediosDifusion', name: 'mediosDifusion',  component: MediosDifusion },
         {path: '/mediosDifusion/add', name: 'addMediosDifusion',  component: addMediosDifusion },    
         {path: '/mediosDifusion/update/:id', name: 'updateMediosDifusion',  component: updateMediosDifusion },  
         
         {path: '/medioContacto', name: 'medioContacto',  component: MedioContacto },
         {path: '/medioContacto/add', name: 'addMedioContacto',  component: addMedioContacto },    
         {path: '/medioContacto/update/:id', name: 'updateMedioContacto',  component: updateMedioContacto }, 
         
         {path: '/olt', name: 'Olt',  component: Olt },
         {path: '/olt/add', name: 'addOlt',  component: addOlt },    
         {path: '/olt/update/:id', name: 'updateOlt',  component: updateOlt }, 
      ]
    }
   
 
  ]
});

//router.beforeEach((to, from, next) => {
  //console.log(to)
/*   if (to.name === 'cancelapago') {
    if (true) {
      next({ path: '/home' })
    }
  }else{
    next();
  } */
  
//});

export default router